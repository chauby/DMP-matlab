% Dynamic Movement Primitives test
%

close all;
clear;

%% load demo trajectory
demo_trajectory = load('Data/demo_trajectory_sine');
demo_trajectory = demo_trajectory.demo_trajectory_sine;

%% intialize the parameters for DMPs
tau = 1.0;
alpha_x = 0.8;
alpha_y = 2.0;
beta_y = alpha_y/4;
run_time = demo_trajectory(end, 1);
demo_data_len = size(demo_trajectory, 1);
dt = run_time / demo_data_len;
n_bfs = 100;
y_start = demo_trajectory(1, 2);
y_goal = demo_trajectory(end, 2);

dmp = DMP(tau, alpha_x, alpha_y, beta_y, n_bfs, y_start, y_goal, run_time, dt);

% training the DMPs by the given demo trajectory
dmp.setDemoTrajectory(demo_trajectory(:, 2:4));
dmp.imitatePath()

fg1 = figure;
set(fg1, 'position', [200,500, 500, 400]);
hold on;
plot(dmp.cs.t_track, dmp.cs.x_track);
plot(dmp.t_centers, dmp.centers, 'ro');
grid on;
title('Centers of Psi');
xlabel('time(s)');
ylabel('x');
legend('x', 'center', 'Location', 'best');
hold off;

fg2 = figure;
set(fg2, 'position', [700,500, 500, 400]);
hold on;
data_len = 1000;
x = linspace(0, 2, data_len);
for i = 1:dmp.n_bfs
    y = exp(-dmp.h(i)*(x - dmp.centers(i)).^2);
    plot(x, y);
end
grid on;
title('Psi activiations');
xlabel('x');
ylabel('y');
hold off;


% repoduce the trajectory by DMPs
[y_track, dy_track, ddy_track] = dmp.reproduce();

% % change the start
dmp.setStart(4);
dmp.setGoal(y_goal);
[y_track1, dy_track1, ddy_track1] = dmp.reproduce();

% change the goal
dmp.setStart(y_start);
dmp.setGoal(2);
[y_track2, dy_track2, ddy_track2] = dmp.reproduce();

% change the start and goal
dmp.setStart(-2);
dmp.setGoal(4);
[y_track3, dy_track3, ddy_track3] = dmp.reproduce();

% change the start and goal
dmp.setStart(4);
dmp.setGoal(-4);
[y_track4, dy_track4, ddy_track4] = dmp.reproduce();

%% plot
fg2 = figure;
set(fg2, 'position', [1200,500, 500, 400]);
hold on;
plot(demo_trajectory(:,1), demo_trajectory(:,2), '--');
plot(dmp.t_track, y_track);
plot(dmp.t_track, y_track1, '.');
plot(dmp.t_track, y_track2, '.');
plot(dmp.t_track, y_track3, '.');
plot(dmp.t_track, y_track4, '.');
grid on;
title('Demo of DMPs');
xlabel('time(s)');
ylabel('y');
legend('demo y', 'y track', 'diff start', 'diff goal', 'diff start and goal 1', 'diff start and goal 2');
hold off;
