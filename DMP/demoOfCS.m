%% first order dynamic system test
close all;
clear all;

% different alpha test
dt = 0.01;
alpha = 2.0;
run_time = 3;
cs = CanonicalSystem(alpha, dt, run_time);
% run one time of canonical system
cs.reset();
cs.rollOut();

%% plot
fg = figure;
set(fg, 'position', [500,400, 500, 500]);
hold on;
plot(cs.t_track, cs.x_track);
plot(cs.t_track, cs.dx_track);
title('Demo of canonical system');
xlabel('time (s)');
ylabel('x');
legend('x', 'dx', 'Location', 'best');
grid on;
hold off;
