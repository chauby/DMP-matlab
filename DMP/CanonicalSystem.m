% The implementation of canonical system
%
% dot(x) = -aplha*x
%

classdef CanonicalSystem < handle
    properties
        alpha; % the decay constant
        x_start; % the default value is 1.0
        run_time;
        t_track;
        dt;
        x;
        dx;
        x_track;
        dx_track;
    end

    methods
        % construct function
        function self = CanonicalSystem(alpha, dt, run_time, x_start)
            % set the default value of run_time and x_start
            if nargin < 3
                run_time = 1.0;
                x_start = 1.0;
            elseif nargin < 4
                x_start = 1.0;
            end

            self.alpha = alpha;
            self.x_start = x_start;
            self.t_track = 0:dt:run_time-dt;
            self.dt = dt;
            self.run_time = run_time;
            self.x = x_start;
            self.dx = -self.alpha*self.x;
            self.x_track = [];
            self.dx_track = [];
        end

        function setAlpha(self, alpha)
            self.alpha = alpha;
        end

        function setStart(self, x_start)
            self.x_start = x_start;
        end

        function setDt(self, dt)
            self.dt = dt;
        end

        function step(self)
            self.dx = -self.alpha*self.x;
            self.x = self.x + self.dt*self.dx;
        end

        function rollOut(self)
            self.reset();
            for t = self.t_track
                if t ~= 0
                    self.step();
                end

                self.x_track = [self.x_track; self.x];
                self.dx_track = [self.dx_track; self.dx];
            end
        end

        function reset(self)
            self.x = self.x_start;
            self.dx = -self.alpha*self.x;
            self.x_track = [];
            self.dx_track = [];
            self.t_track = 0:self.dt:self.run_time-self.dt;
        end
    end
end
