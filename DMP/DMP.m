% The implementaion of discrete DMPs
%
% The differential equations of DMPs can be described as follows
%
% tau*dz = alpha_y(beta_y*(g - y) - z) + f
% tau*dy = z
% tau*dx = -alpha_x*x
%
% or
%
% tau^2*ddy = alpha_y(beta_y*(g - y) - dy) + f
% tau*dx = -alpha_x*x
%
% y: the position of the path
% dy: the velocity of the path
% ddy: the acceleration of the path
% x: the position of the canonical system
% dx: the velocity of the canonical system
% tau: the constant value to achive the temporal scaling
% alpha_y: the decay constant of second dynamic system
% beta_y: the decay constant of second dynamic system
% alpha_x: the decay constant of the canonical system
% f: the forcing term of DMPs, which is a nonlinear function
%

classdef DMP < handle
    properties
        tau;
        alpha_x; % decay constant for canonical system
        alpha_y; % decay constant for DMP dynamic equations
        beta_y;
        n_bfs; % the number of basis functions
        cs;

        centers;
        h; % The variance
        weights;
        psi;
        f_desired; % the desired forcing term path over time

        run_time;
        t_track;
        t_centers;
        dt;

        y_demo; % the acceleration of the demo trajectory
        y_start;
        y_goal;
        start_goal_gap;
        y;
        dy;
        ddy;
        y_track; % the desired position of trajectory over time
        dy_track; % the desired velocity of trajectory over time
        ddy_track; % the desired acceleration of trajectory over time
    end

    methods
        % construct function
        function self = DMP(tau, alpha_x, alpha_y, beta_y, n_bfs, y_start, y_goal, run_time, dt)
            if abs(tau) < 1e-8
                error('The parameter tau can not be 0');
            end

            % if abs(y_start - y_goal) < 1e-8
            %     y_goal = y_start + 1e-6;
            % end

            if nargin < 8
                run_time = 1.0;
                dt = 0.01;
            elseif nargin < 9
                dt = 0.01;
            end

            % the dynamics parameters of DMPs
            self.tau = tau;
            self.alpha_x = alpha_x;
            self.alpha_y = alpha_y;
            self.beta_y = beta_y;
            self.n_bfs = n_bfs;
            self.cs = CanonicalSystem(alpha_x/tau, dt, run_time);
            self.f_desired = [];

            % the time corresponds
            self.run_time = run_time;
            self.dt = dt;
            self.t_track = 0:dt:run_time-dt;
            self.t_centers = linspace(0, self.run_time, self.n_bfs);

            % the trajectory corresponds over time
            self.y_demo = [];
            self.y_start = y_start;
            self.y_goal = y_goal;
            self.start_goal_gap = y_goal - y_start;
            self.y = y_start;
            self.dy = 0;
            self.ddy = 0;
            self.y_track = [];
            self.dy_track = [];
            self.ddy_track = [];

        end

        function setTau(self, tau)
            self.tau = tau;
        end

        function setAlphaX(self, alpha_x)
            self.alpha_x = alpha_x;
            self.cs.setAlpha(alpha_x);
        end

        function setAlphaY(self, alpha_y)
            self.alpha_y = alpha_y;
        end

        function setBetaY(self, beta_y)
            self.beta_y = beta_y;
        end

        function setStart(self, y_start)
            self.y_start = y_start;
        end

        function setGoal(self, y_goal)
            self.y_goal = y_goal;
        end

        function setDemoTrajectory(self, y_demo)
            self.y_demo = y_demo;
        end

        function setNBFs(self, n_bfs)
            self.n_bfs = n_bfs;
        end

        function setDt(self, dt)
            self.dt = dt;
            self.cs.setDt(self.dt);
        end

        % find the centers of psi
        function generateCenters(self)
            self.cs.rollOut();
            centers_index = zeros(length(self.t_centers), 1);
            for i=1:length(self.t_centers)
                % find the centers that is closest to the t_centers
                index = find(abs(self.t_centers(i) - self.cs.t_track) < 1.2*self.dt, 1);
                if ~isempty(index)
                    centers_index(i) = index;
                else
                    error(strcat('DMP generate centers error: can not find t=', num2str(self.cs.t_track(i))));
                end
            end

            self.centers = self.cs.x_track(centers_index);
        end

        % calculate the variance of Psi
        function generateVariance(self)
            self.h = self.n_bfs./self.centers;
        end

        function generatePsi(self)
            x = self.cs.x_track;
            psi = zeros(length(x), length(self.centers));
            for i=1:length(x)
                psi(i,:) = (exp(-self.h.*(x(i) - self.centers).^2))';
            end
            self.psi = psi;
        end

        % before we generating the weights, we need to run rollOut firstly
        function generateWeights(self)
            psi_tmp = self.psi;
            s = self.cs.x_track*self.start_goal_gap;
            fd = self.tau^2*self.y_demo(:,3) - self.alpha_y*(self.beta_y*(self.y_goal - self.y_demo(:,1)) - self.tau*self.y_demo(:,2));
            wi = zeros(size(psi_tmp));

            % wi keeps same for each data point xi
            for i=1:size(wi,1)
                for k = 1:self.n_bfs
                    numer = sum(s.*psi_tmp(:,k).*fd);
                    denom = sum(s.*psi_tmp(:,k).*s);
                    wi(i,k) = numer ./ denom;
                end
            end

            wi(isnan(wi)) = 0;
            self.weights = wi;
        end

        function generateForcingTerm(self)
            % the original DMPs
            % numer = sum(self.psi.*self.weights, 2).*self.cs.x_track*self.start_goal_gap;

            % to solve the problem of reproduce error while g == y_0
            % numer = sum(self.psi.*self.weights, 2).*self.cs.x_track*(self.start_goal_gap/(abs(self.start_goal_gap) + 1e-6));

            % to solve the problem of reproduce error while g == y_0
            A = 0.56*sign(self.y_demo(end,1) - self.y_demo(1,1) + 1e-8)*(max(self.y_demo(:, 1)) - min(self.y_demo(:, 1)));
            numer = sum(self.psi.*self.weights, 2).*self.cs.x_track*A;

            denom = sum(self.psi, 2);
            self.f_desired = numer ./ denom;
        end

        function step(self, force_term)
            self.cs.step();
            self.ddy = self.alpha_y/self.tau*(self.beta_y*(self.y_goal - self.y) - self.dy) + force_term/self.tau;
            self.dy = self.dy + self.ddy*self.dt/self.tau;
            self.y = self.y + self.dy*self.dt;
        end

        function rollOut(self)
            self.reset();
            for t = self.t_track
                % y, dy, ddy will be set to initial value while t=0
                if t ~= 0
                    self.step(0);
                end

                self.y_track = [self.y_track; self.y];
                self.dy_track = [self.dy_track; self.dy];
                self.ddy_track = [self.ddy_track; self.ddy];
            end
        end

        function imitatePath(self)
            self.rollOut();

            % generate centers and variance for psi
            self.generateCenters();
            self.generateVariance();

            % generate psi
            self.generatePsi();

            % generate weights for the forcing term
            self.generateWeights();

            % generate the forcing term
            self.generateForcingTerm();
        end

        function [y_track, dy_track, ddy_track] = reproduce(self)
            self.reset();
            self.cs.rollOut();
            self.generateForcingTerm();

            data_len = length(self.t_track);
            for i = 1:data_len
                self.ddy = self.alpha_y/self.tau*(self.beta_y*(self.y_goal - self.y) - self.dy) + self.f_desired(i)/self.tau;
                self.dy = self.dy + self.ddy*self.dt/self.tau;
                self.y = self.y + self.dy*self.dt;

                self.y_track = [self.y_track; self.y];
                self.dy_track = [self.dy_track; self.dy];
                self.ddy_track = [self.ddy_track; self.ddy];
            end
            y_track = self.y_track;
            dy_track = self.dy_track;
            ddy_track = self.ddy_track;
        end

        function reset(self)
            self.cs.reset();
            self.y = self.y_start;
            self.dy = self.y_demo(1,2);
            self.ddy = self.y_demo(1,3);
            self.start_goal_gap = self.y_goal - self.y_start;
            self.y_track = [];
            self.dy_track = [];
            self.ddy_track = [];
            self.t_track = 0:self.dt:self.run_time - self.dt;
            self.t_centers = linspace(0, self.run_time, self.n_bfs);
        end
    end
end
