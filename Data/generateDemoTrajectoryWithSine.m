%% generate imitate path with sine
close all;
clear;

point_num = 500;
t_demo = linspace(0,5, point_num)';
y_demo = sin(t_demo) + 2*cos(t_demo);
dy_demo = cos(t_demo) - 2*sin(t_demo);
ddy_demo = -sin(t_demo) - 2*cos(t_demo);

fg_demo_sine = figure;
set(fg_demo_sine, 'position', [800,400, 500, 400]);
hold on;
plot(t_demo, y_demo, 'r', 'linewidth', 2);
plot(t_demo, dy_demo);
plot(t_demo, ddy_demo);
xlim([min(t_demo)-1, max(t_demo)+1])
grid on;
title('DMP imitation');
xlabel('time(s)');
ylabel('y');
legend('y', 'dy', 'ddy');
hold off;

%% save the data to files
demo_trajectory_sine = [t_demo, y_demo, dy_demo, ddy_demo];
save('./Data/demo_trajectory_sine','demo_trajectory_sine');
