%% generate the demo trajectory by gaussians

close all;
clear;

run_time = 9;
dt = 0.01;
t = (0:dt:run_time)';
data_len = length(t);

mu=[1, 2, 4, 8, 9]';
sigma=[1, 1, 1.1, 2, 1]';
omega=[0.1, 0.3, 0.2, 0.2, 0.2]';

dime_num = length(mu);
g=zeros(data_len,dime_num);
y=zeros(data_len, 1);

for i=1:dime_num
    g(:, i)=1/(sigma(i)*sqrt(2*pi))*exp(-(t-mu(i)).^2/(2*sigma(i)^2));
    y = y + omega(i)*g(:, i);
end

%% calculate the dy, ddy
dt = run_time / data_len;
dy = zeros(data_len, 1);
for i=2:data_len
    dy(i-1) = (y(i) - y(i-1))/dt;
end
dy(end) = dy(end-1);

ddy = zeros(data_len, 1);
for i=2:data_len
    ddy(i-1) = (dy(i) - dy(i-1))/dt;
end
ddy(end) = ddy(end-1);

%% plot the figure
figure
hold on
for i=1:dime_num
    plot(t, g(:, i),'--')
end
plot(t, y, 'r','LineWidth',2)

%% set the font in figure
legend1=legend('g1', 'g2', 'g3', 'g4', 'g5', 'y', 'location','NorthEast');
set(legend1,'FontName','Times New Roman','FontSize',16);
title('Demo trajectory with GP','FontName','Times New Roman','FontSize',16);
xlim([min(t)-0.5, max(t)+1])
xlabel('t (s)')
ylabel('y (m)')
grid on
hold off

%% save the data to files
demo_trajectory_gaussian = [t, y, dy, ddy];
save('./Data/demo_trajectory_gaussian','demo_trajectory_gaussian');
