%% second order dynamic system test
close all;
clear all;

% different alpha test
dt = 0.01;
tau = 1;
alpha = [2; 1; 3; 6];
beta = alpha/4;
y_start = 1;
y_goal = [0; 1; 3; 4];
sDS = secondOrderDS(tau, alpha(1), beta(1), y_start, y_goal(1), dt);

time = 10;
t_queue = 0:dt:time;
time_steps = length(t_queue);

y_queue = [];
dy_queue = [];
ddy_queue = [];

y_queue_tmp = sDS.y;
dy_queue_tmp = sDS.dy;
ddy_queue_tmp = sDS.ddy;

for i=1:time_steps - 1
    sDS.step();
    y_queue_tmp = [y_queue_tmp; sDS.y];
    dy_queue_tmp = [dy_queue_tmp; sDS.dy];
    ddy_queue_tmp = [ddy_queue_tmp; sDS.ddy];
end

y_queue = [y_queue, y_queue_tmp];
dy_queue = [dy_queue, dy_queue_tmp];
ddy_queue = [ddy_queue, ddy_queue_tmp];

%% plot
fg = figure;
set(fg, 'position', [500,400, 500, 500]);
hold on;
plot(t_queue, y_queue);
plot(t_queue, dy_queue);
plot(t_queue, ddy_queue);
title('Demo of second oder dynamic system');
% xlim([min(t_queue) - 0.1, max(t_queue) + 0.2]);
% ylim([min(min(y_queue)) - 0.1, max(max(y_queue)) + 0.1]);
xlabel('time (s)');
ylabel('x');
% legend(label, 'Location', 'best')
legend('y', 'dy', 'ddy');
grid on;
hold off;
