%% the second order dynamic system
% the 2nd order dynamic system can be implemented by two 1st order dynamic system

close all;
clear all;

dt = 0.01;
tau = 1;
alpha = [1; 3; 10; 6];
% for a critically damped spring-damper system, c=sqrt(m*k), and then alpha = 2*sqrt(alpha*beta), such that beta = alpha/4
beta = alpha/4;
y_start = 2;
y_goal = [0; 1; 3; 4];
sDS = secondOrderDS(tau, alpha(1), beta(1), y_start, y_goal(1), dt);

time = 15;
t_queue = 0:dt:time;
time_steps = length(t_queue);

y_queue = [];
dy_queue = [];
ddy_queue = [];

y_queue_tmp = sDS.y;
dy_queue_tmp = sDS.dy;
ddy_queue_tmp = sDS.ddy;

% different alpha
for i=1:size(alpha, 1)
    sDS.setAlpha(alpha(i));
    sDS.setBeta(beta(i));
    sDS.reset();
    y_queue_tmp = sDS.y;
    dy_queue_tmp = sDS.dy;
    ddy_queue_tmp = sDS.ddy;

    for k=1:time_steps - 1
        sDS.step();
        y_queue_tmp = [y_queue_tmp; sDS.y];
        dy_queue_tmp = [dy_queue_tmp; sDS.dy];
        ddy_queue_tmp = [ddy_queue_tmp; sDS.ddy];
    end

    y_queue = [y_queue, y_queue_tmp];
    dy_queue = [dy_queue, dy_queue_tmp];
    ddy_queue = [ddy_queue, ddy_queue_tmp];
end

% different goals
for j=2:size(y_goal, 1)
    sDS.setGoal(y_goal(j));
    sDS.reset();
    y_queue_tmp = sDS.y;
    dy_queue_tmp = sDS.dy;
    ddy_queue_tmp = sDS.ddy;

    for k=1:time_steps - 1
        sDS.step();
        y_queue_tmp = [y_queue_tmp; sDS.y];
        dy_queue_tmp = [dy_queue_tmp; sDS.dy];
        ddy_queue_tmp = [ddy_queue_tmp; sDS.ddy];
    end

    y_queue = [y_queue, y_queue_tmp];
    dy_queue = [dy_queue, dy_queue_tmp];
    ddy_queue = [ddy_queue, ddy_queue_tmp];
end

%% plot
fg = figure;
set(fg, 'position', [400,400, 500, 500]);
hold on;
plot(t_queue, y_queue(:,1));
plot(t_queue, dy_queue(:,1));
plot(t_queue, ddy_queue(:,1));
title('Demo of second order dynamic system');
xlabel('time (s)');
ylabel('y');
legend('y', 'dy', 'ddy');
grid on;
hold off;

%% plot different alpha and different goal state
fg = figure;
set(fg, 'position', [1000,400, 500, 500]);
hold on;
% plot different alphas with a same goal state
label = cell(size(alpha, 1), 1);
for i=1:size(alpha, 1)
    plot(t_queue, y_queue(:, i));
    label{i} = strcat(strcat('alpha=',  num2str(alpha(i))), strcat(', goal=', num2str(y_goal(1))));
end

% plot different goal states with a same alpha
for j=2:size(y_goal, 1)
    plot(t_queue, y_queue(:, j+i-1));
    label{j+i-1} = strcat(strcat('alpha=',  num2str(alpha(i))), strcat(', goal=', num2str(y_goal(j))));
end

title('Demo of second order dynamic system');
xlim([min(t_queue) - 0.1, max(t_queue) + 0.2]);
ylim([min(min(y_queue)) - 0.1, max(max(y_queue)) + 0.1]);
xlabel('time (s)');
ylabel('y');
legend(label, 'Location', 'best');
grid on;
hold off;
