%% first order dynamic system test
close all;
clear all;

% different alpha test
dt = 0.01;
alpha = [0.1; 1; 3; 6];
x_start = 2;
x_goal = [0; 1; 3; 4];
fDS = firstOrderDS(alpha(1), x_start, x_goal(1), dt);

time = 5;
x_queue = [];
x_queue_tmp = [];
t_queue = 0:dt:time;
time_steps = length(t_queue);

% different alpha
for i=1:size(alpha, 1)
    fDS.setAlpha(alpha(i));
    fDS.reset();
    x_queue_tmp = fDS.x;

    for k=1:time_steps - 1
        fDS.step();
        x_queue_tmp = [x_queue_tmp; fDS.x];
    end

    x_queue = [x_queue, x_queue_tmp];
end

% different goal state
for j = 2:size(x_goal, 1)
    fDS.setGoal(x_goal(j));
    fDS.reset();
    x_queue_tmp = fDS.x;

    for k=1:time_steps - 1
        fDS.step();
        x_queue_tmp = [x_queue_tmp; fDS.x];
    end

    x_queue = [x_queue, x_queue_tmp];
end

%% plot
fg = figure;
set(fg, 'position', [500,400, 500, 500]);
hold on;
% plot different alphas with a same goal state
label = cell(size(alpha, 1), 1);
for i=1:size(alpha, 1)
    plot(t_queue, x_queue(:, i));
    label{i} = strcat(strcat('alpha=',  num2str(alpha(i))), strcat(', goal=', num2str(x_goal(1))));
end

% plot different goal states with a same alpha
for j=2:size(x_goal, 1)
    plot(t_queue, x_queue(:, j+i-1));
    label{j+i-1} = strcat(strcat('alpha=',  num2str(alpha(i))), strcat(', goal=', num2str(x_goal(j))));
end

title('Demo of first order dynamic system');
xlim([min(t_queue) - 0.1, max(t_queue) + 0.2]);
ylim([min(min(x_queue)) - 0.1, max(max(x_queue)) + 0.1]);
xlabel('time (s)');
ylabel('x');
legend(label, 'Location', 'best')
grid on;
hold off;
