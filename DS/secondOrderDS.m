% The second order dynamic system
%
% dot(z) = -alpha*(y - y_g) - beta*z     
% dot(y) = z                                               
%
% let x = [y, z]^T
% then dot(x) = [dot(y), dot(z)]^T, x_g = [y_g, 0]^T
% after adding the tau, y_g
%
% tau*dot(y) = z
% tau*dot(z) = alpha(beta(y_g - y) - z)
%
% dot(y) = z/tau
% dot(z) = (alpha(beta(y_g - y) - z))/tau
%

classdef secondOrderDS < handle
    properties
        tau;
        alpha;
        beta;
        t;
        dt;
        y_start;
        y_goal;
        y;
        dy;
        ddy;
        z;
        dz;
    end

    methods
        % construct function
        function self = secondOrderDS(tau, alpha, beta, y_start, y_goal, dt)
            if abs(tau - 0.0) < 1e-8
                error('The parameter tau can not be 0');
            end
            self.tau = tau;
            self.alpha = alpha;
            self.beta = beta;
            self.t = 0.0;
            self.dt = dt;
            self.y_start = y_start;
            self.y_goal = y_goal;
            self.y = self.y_start;
            self.dy = 0.0;
            self.z = self.tau*self.dy;
            self.dz = self.alpha/self.tau*(self.beta*(self.y_goal - self.y) - self.z);
            self.ddy = self.dz/self.tau;
        end

        function setTau(self, tau)
            self.tau = tau;
        end

        function setAlpha(self, alpha)
            self.alpha = alpha;
        end

        function setBeta(self, beta)
            self.beta = beta;
        end

        function setStart(self, y_start)
            self.y_start = y_start;
        end

        function setGoal(self, y_goal)
            self.y_goal = y_goal;
        end

        function setDt(self, dt)
            self.dt = dt;
        end

        function step(self)
            self.dz = self.alpha/self.tau*(self.beta*(self.y_goal - self.y) - self.z);
            self.z = self.z + self.dt*self.dz;
            self.dy = self.z/self.tau;
            self.y = self.y + self.dt*self.dy;
            self.ddy = self.dz/self.tau;
            self.t = self.t + self.dt;
        end

        function reset(self)
            self.t = 0.0;
            self.y = self.y_start;
            self.dy = 0.0;
            self.z = self.tau*self.dy;
            self.dz = self.alpha/self.tau*(self.beta*(self.y_goal - self.y) - self.z);
            self.ddy = self.dz/self.tau;
        end
    end
end
