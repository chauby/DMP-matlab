% The first order dynamic system can be described and implemented 
% as follws
% 
% dot(x) = -aplha*(x - x_g)
%
classdef firstOrderDS < handle
    properties
        alpha; % decay constant
        x_start; % start state
        x_goal; % goal state
        t;
        dt;
        x;
        dx;
    end

    methods
        % construct function
        function self = firstOrderDS(alpha, x_start, x_goal, dt)
            self.alpha = alpha;
            self.x_start = x_start;
            self.x_goal = x_goal;
            self.dt = dt;
            self.t = 0;
            self.x = x_start;
            self.dx = -self.alpha*(self.x - self.x_goal);
        end

        function setAlpha(self, alpha)
            self.alpha = alpha;
        end

        function setStart(self, x_start)
            self.x_start = x_start;
        end

        function setGoal(self, x_goal)
            self.x_goal = x_goal;
        end

        function setDt(self, dt)
            self.dt = dt;
        end

        function step(self)
            self.dx = -self.alpha*(self.x - self.x_goal);
            self.x = self.x + self.dt*self.dx;
            self.t = self.t + self.dt;
        end

        function reset(self)
            self.t = 0;
            self.x = self.x_start;
            self.dx = -self.alpha*(self.x - self.x_goal);
        end
    end
end
